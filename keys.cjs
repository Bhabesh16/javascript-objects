function keys(obj) {
    if (typeof obj !== 'object') return [];
    
    let keysList = [];
    
    for (let key in obj) {
        keysList.push(key);
    }
    
    return keysList;
}

module.exports = keys;