function values(obj) {
    if (typeof obj !== 'object') return [];
    
    let valuesList = [];
    
    for (let key in obj) {
        valuesList.push(obj[key]);
    }
    
    return valuesList;
}

module.exports = values;