const { testObject } = require('./testObject.cjs');
const defaults = require('../defaults.cjs');

const defaultProps = {
    name: 'Bruce Wayne',
    superhero: 'Batman',
    dateofbirth: '7 April 1915'
};

const result = defaults(testObject, defaultProps);
console.log(result);