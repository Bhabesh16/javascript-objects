function defaults(obj, defaultProps) {
    if (typeof obj !== 'object' || typeof defaultProps !== 'object') return [];
    
    for (let key in defaultProps) {
        if (obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    }
    
    return obj;
}

module.exports = defaults;